fun main() {
    val child = Person("Child", "Child", 1)
    val p1 = Person("Tom", "Jones", 35, child)
    println(p1.firstName)

    val rect1 = Rectangle(5.0, 2.0)
    println("The perimeter is ${rect1.perimeter}")

    val rect2 = Rectangle(5.0, 2.0)
    println(rect1 == rect2) // true
}

class Person(val firstName: String, val lastName: String, var age: Int) {
    var children: MutableList<Person> = mutableListOf() // ArrayList

    init {
        println("Person is created $firstName")
    }

    constructor(firstName: String, lastName: String, age: Int, child: Person):
            this(firstName, lastName, age) {
        children.add(child)
    }

    // конструктор без аргументов
    constructor(): this("", "", -1)
}

data class Rectangle(var height: Double, var length: Double) {
    var perimeter = (height + length) * 2

    var test = 1
        get() = field + 1 // this.поле
        set(value) {
            if (value < 0) println("Negative value")
            field = value
        }

    fun area() = height * length
}