fun main() {
    var a: String = "Hello"
    //a = null // NPE исключены

    var b: String? = "Test"
    b = null

    // b.length

    // вернет длину b, если null -> null (Никаких NPE!)
    b?.length

    // bob?.department?.head?.name -> null (Никаких NPE!)

    // Elvis оператор - ?:
    val l = b?.length ?: -1

    b = if ((0..10).random() > 5) "asgt" else null
    // !! - выбрасывает NPE в случае если в переменной null
    val t = b!!.length
}