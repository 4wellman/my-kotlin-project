fun main(args: Array<String>) {
    var name: String = "Miha"

    var a: Int = 1;
    var b: Int = 2;

    println("Hello $name!")
    println("A + B = ${a+b}")

    val items = listOf("apple", "banana", "orange")

    for (item in items)
        println(item)

    //while
    var index = 0
    while (index < items.size) {
        println("Item at $index is ${items[index]}")
        index++
    }
    println()

    println(5 in 3..10)
    println()

    for (i in 1..10)
        println(i)
    println()

    for (i in 1 until 10)
        println(i)
    println()

    for (i in 10 downTo 1)
        println(i)
    println()

    for (i in 0..100 step 10)
        println(i)

}