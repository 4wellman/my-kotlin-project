// listeners for EditText
fun main(args: Array<String>) {

    var eText = findViewById<EditText>(R.id.editText_search2)

    eText.doOnTextChanged { text, start, count, after ->
        Log.d("STATE", "doOnTextChanged: $text, $start, $count, $after")
    }

    /*eText.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            Log.d("STATE", "beforeTextChanged: ${s.length}, $count")
            if( s.isNotEmpty() )
                Log.d("STATE", "beforeTextChanged: ${findViewById<EditText>(R.id.editText_search2).text}")
        }
        override fun afterTextChanged(s: Editable) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })*/

    eText.setOnFocusChangeListener { view, hasFocus ->
        if (hasFocus) {
            Log.d("STATE", "catch focus: ${findViewById<EditText>(R.id.editText_search2).text}")
        } else {
            Log.d("STATE", "lost focus: ${findViewById<EditText>(R.id.editText_search2).text}")
        }
    }

    eText.setOnKeyListener { v, keyCode, event ->
        if (event.action == KeyEvent.ACTION_UP) {
            Log.d("STATE", "onKeyUp: ${findViewById<EditText>(R.id.editText_search2).text}")

            return@OnKeyListener true
        }
        false
    }

    eText.setOnClickListener { v ->
        Log.d("STATE", "onClick: ${findViewById<EditText>(R.id.editText_search2).text}")
    }

}
